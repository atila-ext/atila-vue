class BaseAuthHandler:
    def __init__ (self, access_token_due, refresh_token_due):
        self._access_token_due = access_token_due
        self._refresh_token_due = refresh_token_due

    def get_access_token_due (self):
        return self._access_token_due

    def get_refresh_token_due (self):
        return self._refresh_token_due

    def create_account (self, email, password = None):
        raise NotImplementedError

    def delete_account (self, user):
        raise NotImplementedError

    def get_account (self, email, password = None):
        raise NotImplementedError

    def get_jwt_payload (self, user):
        raise NotImplementedError

    def set_email_verified (self, user):
        raise NotImplementedError


class ExampleUser:
    def __init__ (self, uid, email, password = None):
        self.uid = uid
        self.email = email
        self.password = password
        self.grp = ['user']
        self.status = 'ACTIVE'
        self.email_verified = False

class ExampleAuthHandler (BaseAuthHandler):
    def __init__ (self, access_token_due, refresh_token_due):
        self.userlist = {}
        super ().__init__ (access_token_due, refresh_token_due)

    def create_account (self, email, password = None):
        uid = len (self.userlist) + 1
        self.userlist [email] = ExampleUser (uid, email, password)
        return self.userlist [email]

    def get_account (self, email, password = None):
        user = self.userlist.get (email)
        if not user:
            return
        if password and user.password != password:
            return
        return user

    def delete_account (self, user):
        user.status = 'DELETED'

    def get_jwt_payload (self, user):
        return dict (uid = user.uid, grp = user.grp, email = user.email)

    def set_email_verified (self, user):
        if user.email_verified:
            return
        user.email_verified = True
