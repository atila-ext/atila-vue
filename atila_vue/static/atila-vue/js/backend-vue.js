axios.interceptors.request.use ( async (config) => {
  if (!("Authorization" in config.headers)) {
    return config
  }
  const atk = config.headers.Authorization
  if (!atk) {
    return config
  }
  if (typeof (store.state.$refresh_token_api) == 'string' && config.url == backend.endpoint (store.state.$refresh_token_api)) {
    return config
  }
  if (config.url.substring (0, 1) != '/') {
    return config
  }

  if (!!store.state.$refresh_token_api) {
    let exp
    if (!!store.state.$claims) {
      exp = store.state.$claims.exp
    } else {
      exp = decode_jwt (atk.substring (7, atk.length)).exp
    }
    if (exp < new Date ().unixepoch () + 120) {
      frontend.log (`refreshing token: vaild in ${exp - new Date ().unixepoch ()} seconds.`, 'debug')
      let access_token
      if (typeof (store.state.$refresh_token_api) == 'function') {
        access_token = await store.state.$refresh_token_api ()
      } else {
        access_token = await backend.refresh_access_token ()
      }
      if (!!access_token) {
        config.headers.Authorization = `Bearer ${ access_token }`
      }
    }
  }
  return config
}, (error) => {
  return Promise.reject (error)
})

axios.defaults.withCredentials = true














class AsynWebSocket extends WebSocket {
  constructor (url, read_handler = (evt) => frontend.log (evt.data)) {
    super (url)
    this.onmessage = read_handler
    this.buffer = []
    this.connected = false
  }

  onwrite() {
    for (var i = 0; i < this.buffer.length; i++) {
      msg = this.buffer.shift ()
      frontend.log (`send: ${ msg }`, 'websocket')
      this.sock.send (msg)
    }
  }

  onopen() {
    this.connected = true
    frontend.log ('connected', 'websocket')
    this.onwrite ()
  }

  onclose (evt) {
    this.connected = false
    frontend.log ('closed', 'websocket')
  }

  push(msg) {
    if (!msg) { return }
    this.buffer.push (msg)
    if (!this.connected) {
      return
    }
    this.onwrite ()
  }
}











class HttpResponse {
  constructor (response, errorText) {
    this.response = response
    this.errorText = errorText
    this.error = !!errorText

    if (typeof (response) == 'number') {
      this.status_code = response
      this.status = 'Request Error'
      this.data = {
        code: parseInt (`${this.status_code}00`),
        message: 'request error',
        more_info: errorText
      }
    } else {
      this.status_code = response.status
      this.status = response.statusText
      this.data = response.data
    }
    this.name = "HttpResponse"
  }
}

















const backend = {
  async refresh_access_token (_name = null) {
    if (!!_name) { // initial set
      store.state.$refresh_token_api = _name
    }

    const name = store.state.$refresh_token_api
    if (typeof (name) == 'function') { // custom functional access token
      if (name.constructor.name != 'AsyncFunction') {
        throw Error (`${name.name} should be async function`)
      }
      return
    }

    if (!name) {
      frontend.log ('no initial call refresh_access_token (API_ID or function)', 'warn')
      return
    }
    const access_token = store.state.$ls.get ('access_token')
    if (!access_token) {
      store.commit ('_clear_credential')
      return
    }

    let claim = frontend.decode_jwt (access_token)
    if (!claim) { // corrupted token
      store.commit ('_clear_credential')
      return
    }

    if (claim.exp > new Date ().unixepoch () + 120) { // over 2 minutes to expiration
      if (!_name) {
        return // this is called by axio hook, nothing to do, all is ok
      }
      store.commit ("_save_credential", {uid: claim.uid, grp: claim.grp, access_token})
      return access_token
    }

    const rtk = store.state.$ls.get ('refresh_token')
    if (!rtk) {
      store.commit ('_clear_credential')
      return
    }

    axios.defaults.headers.common ["Authorization"] = `Bearer ${access_token}`
    const r = await this._process_credential (name, {refresh_token: rtk})
    if (r.data.access_token == null) {
      store.commit ("_save_credential", {uid: claim.uid, grp: claim.grp, access_token, refresh_token: r.data.refresh_token})
      return access_token
    } else {
      claim = frontend.decode_jwt (r.data.access_token)
      store.commit ("_save_credential", {uid: claim.uid, grp: claim.grp, access_token: r.data.access_token, refresh_token: r.data.refresh_token})
      return r.data.access_token
    }
  },

  async get_access_and_refresh_tokens (name, payload) {
    const r = await this._process_credential (name, payload)
    if (r.error) return r
    const claim = frontend.decode_jwt (r.data.access_token)
    store.commit ("_save_credential", {uid: claim.uid, grp: claim.grp, access_token: r.data.access_token, refresh_token: r.data.refresh_token})
    return r
  },

  async remove_access_and_refresh_tokens (name) {
    let r = null
    if (!!name) {
      r = await this._process_credential (name, {})
      if (r.error) return r
    }
    store.commit ('_clear_credential') // local signout
    store.state.$waiting = false
    return r
  },













  _create_failesafe_websocket (buffer = []) {
    const ws = store.state.$websocket
    ws.sock = new AsynWebSocket (ws.url, ws.read_handler)
    ws.sock.buffer = buffer
    ws.push = ws.sock.push
    frontend.log ('connecting...', 'websocket')

    ws.sock.onerror = function (e) {
      frontend.log ('error occurred, try to reconnect...', 'websocket')
      const buffer = [...ws.sock.buffer]
      ws.sock.close ()
      this._create_failesafe_websocket (buffer)
    }
  },

  create_websocket (endpoint, read_handler) {
    const ws = store.state.$websocket
    ws.url = endpoint
    ws.read_handler = read_handler
    this._create_failesafe_websocket ([])
    return ws
  },













  // URL Building -----------------------------------------
  _check_url (url) {
    if (url.substring (0, 1) == '/') {
      throw new Error ('url cannot be started with /')
    }
  },

  _validate (name, params) {
    frontend.validator.hide_feedback ()
    const target = this.get_spec (name)
    if (!('argspecs' in target)) return true
    if (!('ARGS' in target.argspecs)) return true

    const validators = target.argspecs.ARGS
    try {
      frontend.validator.validate (params, validators)
    } catch (e) {
      store.state.$waiting = false
      if (e.status_code == 901) {
        frontend.validator.show_feedback (e)
        return false
      } else {
        frontend.traceback (e)
        return false
      }
    }
    return true
  },

  _build_url_and_payload (name, kargs = {}) {
    if (!this._validate (name, kargs)) return {url: null, payload: null}
    const target = this.get_spec (name)
    const url = this.endpoint (name, kargs)
    if (typeof (target.params) == 'undefined') {
      target.params = []
    }
    const payload = {}
    for (let k in kargs) {
      if (target.params.indexOf (k) != -1) continue
      payload [k] = kargs [k]
    }
    return {url, payload}
  },

  // render url and validating
  _build_url (name, kargs = {}) {
    const {url, payload} = this._build_url_and_payload (name, kargs)
    if (url == null) return null
    let newquery = ''
    for (let k in payload) {
      let v = payload [k]
      if (v === null || v === undefined) { // prefer blank at querystring
        v = ''
      }
      if (!!newquery) {
        newquery += '&'
      }
      newquery += k + "=" + encodeURIComponent (v + '')
    }
    if (!!newquery) {
      return url + "?" + newquery
    }
    return url
  },

  // axios alias
  async _request_with_axios (coro) {
    let resp  = null
    try {
      resp = await coro
      store.state.$waiting = false
      return new HttpResponse (resp)
    } catch (e) {
      store.state.$waiting = false
      const tb_info = frontend.traceback (e)
      if (e.response !== undefined) {
        resp = new HttpResponse (e.response, tb_info)
      } else {
        resp = new HttpResponse (700, tb_info)
      }
      return resp
    }
  },

  _check_method (name, method) {
    if (this.get_spec (name).methods.indexOf (method.toUpperCase ()) == -1) {
      throw new Error ('Method Not Allowed')
    }
  },

  async _process_credential (name, payload) {
    const resp = await this.post (name, payload)
    if (resp.status_code == 401 || resp.status_code == 403) {
      frontend.log (`credential token processing failed: ${resp.status_code}`, 'debug')
      store.commit ('_clear_credential')
    }
    return resp
  },




















  // just render base url
  endpoint (name, kargs = {}) {
    if (!name) {
      return
    }
    const target = this.get_spec (name)
    if (typeof (target.params) == 'undefined') {
      target.params = []
    }

    let url = target.path
    for (let k of target.params) {
      if (kargs [k] === undefined ) {
        url = url.replace ("/:" + k, '')
      } else {
        url = url.replace (":" + k, kargs [k])
      }
    }
    return url
  },

  get_spec (name) {
    const specs = {...store.state.$apispecs, ...store.state.$urlspecs}
    if (name in specs) {
      return specs [name]
    }
    const matched = []
    for (let api_id in specs) {
      if (api_id.endsWith (name)) {
        matched.push (api_id)
      }
    }
    if (matched.length == 1) {
      return specs [matched [0]]
    }
    if (matched.length > 1) {
      throw new Error (`ambiguous route ${name}: [${matched}]`)
    }
    throw new Error (`route ${name} not found`)
  },

  static (relurl) {
    this._check_url (relurl)
    return store.state.$static_url + relurl
  },

  media (relurl) {
    this._check_url (relurl)
    return store.state.$media_url + relurl
  },

  pretest (method, API_ID, data) {
    this._check_method (API_ID, method.toLowerCase ())
    const url = this._build_url (API_ID, data)
    if (url == null) {
      frontend.log (`${API_ID}: 'validation failed`, 'debug')
      return false
    }
    return true
  },

  async get (API_ID, querystring = {}, config = {}, method = 'get') {
    this._check_method (API_ID, method)
    if (config.setWaiting !== false) {
      store.state.$waiting = true
    }
    const url = this._build_url (API_ID, querystring)
    if (url == null) {
      frontend.log (`${method.toUpperCase ()} ${API_ID}: 'validation failed`, 'debug')
      return new HttpResponse (701, 'Parameter Validation Failed') // param validation failed
    }
    return await this._request_with_axios (axios [method] (url, config))
  },

  async download (API_ID, querystring = {}, config = {}, method = 'get') {
    config.responseType = 'blob'
    const response = await this.get (API_ID, querystring, config)

    const link = document.createElement ('a')
    const href = URL.createObjectURL (response.data)
    link.href = href

    let filename = ''
    const dposition = response.response.headers ['content-deposition']
    const s = dposition.indexOf ('filename=')
    filename = dposition.substr (s + 9)
    if (filename.indexOf (";") != -1) {
      const e = filename.indexOf (';')
      filename = filename.substr (0, e)
    }
    if (filename.substr (0, 1) == `"` || filename.substr (0, 1) == `'`) {
      filename = filename.substr (1, filename.length - 2)
    }
    link.setAttribute ('download', filename)
    document.body.appendChild (link)
    link.click ()
    document.body.removeChild (link)
    URL.revokeObjectURL (href)
  },

  async delete (API_ID, querystring = {}, config = {}) {
    return await this.get (API_ID, querystring, config, 'delete')
  },

  async post (API_ID, data = {}, config = {}, method = 'post') {
    this._check_method (API_ID, method)
    if (config.setWaiting !== false) {
      store.state.$waiting = true
    }
    const {url, payload} = this._build_url_and_payload (API_ID, data)
    if (url == null) {
      frontend.log (`${method.toUpperCase ()} ${API_ID}: 'validation failed`, 'debug')
      return new HttpResponse (701, 'Parameter Validation Failed') // param validation failed
    }

    if ('headers' in config && config.headers ['Content-Type'] == 'multipart/form-data') {
      const form = new FormData ()
      for (let k in payload) {
        form.append (k, payload [k])
      }
      return await this._request_with_axios (axios [method] (url, form, config))
    } else {
      return await this._request_with_axios (axios [method] (url, payload, config))
    }
  },

  async patch (API_ID, data = {}, config = {}) {
    return await this.post (API_ID, data, config, 'patch')
  },

  async put (API_ID, data = {}, config = {}) {
    return await this.post (API_ID, data, config, 'put')
  },
}

window.backend = backend
