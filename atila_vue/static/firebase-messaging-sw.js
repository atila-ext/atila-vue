importScripts('/firebase-config.js')
importScripts('https://www.gstatic.com/firebasejs/9.18.0/firebase-app-compat.js')
importScripts('https://www.gstatic.com/firebasejs/9.18.0/firebase-messaging-compat.js')

firebase.initializeApp(firebaseConfig)
const messaging = firebase.messaging()

self.addEventListener('notificationclick', function(event) {
    event.notification.close()
    event.waitUntil(clients.matchAll({
      type: 'window'
    }).then(function(clientList) {
      for (let i = 0; i < clientList.length; i++) {
        const client = clientList[i]
        if (client.url === '/' && 'focus' in client) {
          return client.focus()
        }
      }
      if (clients.openWindow) {
        return clients.openWindow(event.notification.data)
      }
    }))
})

messaging.onBackgroundMessage((msg) => {
    console.log('[firebase-messaging-sw.js] Received background message', msg)
    if (("notification" in msg)) return // already handled by `messaging`
    if (!("data" in msg)) {
        console.log('[firebase-messaging-sw.js] data not found', msg)
        return
    }
    const title = msg.data.title;
    const options = {
        body: msg.data.body,
        data: msg.data.link,
        icon: '/icon/128.png',
    }
    self.registration.showNotification(title, options);
})
