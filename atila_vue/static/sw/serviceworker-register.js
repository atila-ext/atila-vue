if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        let updated = false
        let activated = false
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            console.log('[serviceworker] registration successful with scope', registration.scope)
            registration.addEventListener('updatefound', () => {
                const newWorker = registration.installing
                console.log('[serviceworker] service worker update found')
                newWorker.addEventListener('statechange', () => {
                    console.log('[serviceworker] service worker state changed:', newWorker.state)
                    if (newWorker.state === "activated") {
                        activated = true
                        checkUpdate ()
                    }
                })
            })
        }, function(err) {
            console.log('[serviceworker] registration failed', err)
        })

        navigator.serviceWorker.addEventListener ('controllerchange', () => {
            console.log('[serviceworker] controller changed')
            updated = true
            checkUpdate()
        })

        function checkUpdate () {
            if (activated && updated) {
                console.log("[serviceworker] application was updated refreshing the page...")
                window.location.reload()
            }
        }
    })
}