const SWCONFIG = {
  LOGGING: true,
  DEV_HOSTS: [],

  ENABLE_CACHE_HIT_LOG: false,
  ENABLE_DEV_HOSTS_CACHE: false,

  CACHE_VERSION: "<<COMMIT_SHA>>",
  CDN_CACHE_VERSION: "v1",

  URL_TO_CACHE: [
      '/sw/offline.html',
      '/sw/img/rabbit.png',
  ], // exact URLs

  LIB_CACHE_PATHES: ['/cdn/'],
  LIB_CACHE_MIMETYPES: [],

  CORS_CACHE_PATHES: [],
  CORS_CACHE_MIMETYPES: [],

  NOCAHCE_PATHES: [],
  NOCAHCE_MIMETYPES: [],

  CACHE_PATHES: [],
  CACHE_MIMETYPES: [],
}








// ==================================================================
// DO NOT EDIT BELOW
// ==================================================================


if (!("ENABLE_CACHE_HIT_LOG" in SWCONFIG)) {SWCONFIG.ENABLE_CACHE_HIT_LOG = false}
if (!("ENABLE_DEV_HOSTS_CACHE" in SWCONFIG)) {SWCONFIG.ENABLE_DEV_HOSTS_CACHE = false}
if (!("LOGGING" in SWCONFIG)) {SWCONFIG.LOGGING = false}

if ("CORS_CACHE_VERSION" in SWCONFIG) {
  log ('WARNING: CORS_CACHE_VERSION is deprecated. use CDN_CACHE_VERSION')
  SWCONFIG.CDN_CACHE_VERSION = SWCONFIG.CORS_CACHE_VERSION
}
if (!("CDN_CACHE_VERSION" in SWCONFIG)) {
  SWCONFIG.CDN_CACHE_VERSION = 'v1'
}

if (!("CACHE_VERSION" in SWCONFIG)) {
  log ('WARNING: LOCAL_CACHE_VERSION is deprecated. use CACHE_VERSION')
  SWCONFIG.CACHE_VERSION = SWCONFIG.LOCAL_CACHE_VERSION
}

if (!("LIB_CACHE_PATHES" in SWCONFIG)) {
  SWCONFIG.LIB_CACHE_PATHES = ['/cdn/']
  SWCONFIG.LIB_CACHE_MIMETYPES = []
}

const ARRAY_KEYS = [
  'DEV_HOSTS', 'URL_TO_CACHE',
  'CACHE_PATHES', 'CACHE_MIMETYPES', 'NOCAHCE_PATHES', 'NOCAHCE_MIMETYPES', 'CORS_CACHE_PATHES', 'CORS_CACHE_MIMETYPES'
]
for (let k of ARRAY_KEYS) {
  if (!(k in SWCONFIG) || !SWCONFIG [k]) {
    SWCONFIG [k] = []
  }
}

// caching -----------------------------------------
let USE_CACHE = true

const PRE_CACHE_NAME = `gih-cache-pre-${SWCONFIG.CACHE_VERSION}`
const DYN_CACHE_NAME = `gih-cache-dyn-${SWCONFIG.CACHE_VERSION}`
const COR_CACHE_NAME = `gih-cache-cor-${SWCONFIG.CDN_CACHE_VERSION}`
const LIB_CACHE_NAME = `gih-cache-lib-${SWCONFIG.CDN_CACHE_VERSION}`

if (location.hostname == 'localhost' || SWCONFIG.DEV_HOSTS.indexOf (location.hostname) > -1) {
  SWCONFIG.LOGGING = true
  if (!SWCONFIG.ENABLE_DEV_HOSTS_CACHE) {
    USE_CACHE = false
  }
}

function log (msg) {
  if (SWCONFIG.LOGGING) {
    console.log(`[sw.js] ${ msg }`)
  }
}

// config ---------------------------------------------
let DYN_NOCACHE = {
  enable: true,
  pathes: [ //no cache by path
    /\/sw\.js$/i,
    ...SWCONFIG.NOCAHCE_PATHES
  ],
  mimetypes: SWCONFIG.NOCAHCE_MIMETYPES
}

let LIB_CACHE = { // static lib files
  enable: (SWCONFIG.LIB_CACHE_PATHES.length > 0 || SWCONFIG.LIB_CACHE_MIMETYPES.length > 0),
  pathes: SWCONFIG.LIB_CACHE_PATHES,
  mimetypes: SWCONFIG.LIB_CACHE_MIMETYPES
}

let DYN_CACHE = { // static files
  enable: (SWCONFIG.CACHE_PATHES.length > 0 || SWCONFIG.CACHE_MIMETYPES.length > 0),
  pathes: SWCONFIG.CACHE_PATHES,
  mimetypes: SWCONFIG.CACHE_MIMETYPES
}

let CORS_CACHE = { // external static files
  enable: (SWCONFIG.CORS_CACHE_PATHES.length > 0 || SWCONFIG.CORS_CACHE_MIMETYPES.length > 0),
  pathes: SWCONFIG.CORS_CACHE_PATHES,
  mimetypes: SWCONFIG.CORS_CACHE_MIMETYPES
}
// end of config --------------------------------------

function matched (config, url, content_type) {
  if (config.enable === false) return false
  if (config.pathes.filter (re => url.match (re)).length) return true
  if (config.mimetypes.filter (mtype => content_type.startsWith (mtype)).length) return true
  return false
}

self.addEventListener('install', function(event) {
  if (!USE_CACHE) {
    log (`updating service worker without waiting`)
    self.skipWaiting() // devel mode
  }
  event.waitUntil(
    caches.open(PRE_CACHE_NAME).then(function(cache) {
      log (`cache opened: ${PRE_CACHE_NAME}`)
      if (USE_CACHE && "URL_TO_CACHE" in SWCONFIG) {
        return cache.addAll(SWCONFIG.URL_TO_CACHE)
      }
    })
  )
})











self.addEventListener('activate', function(event) {
  let CACHES_TO_PRESERVE = [COR_CACHE_NAME, LIB_CACHE_NAME, PRE_CACHE_NAME, DYN_CACHE_NAME]
  if (!USE_CACHE) {
    CACHES_TO_PRESERVE.splice (2, 2) // only keep COR_CACHE_NAME
  }
  event.waitUntil(
    caches.keys().then(function(cacheList) {
      return Promise.all(
        cacheList.map(function(cacheName) {
          log (`checking cache ${ cacheName }`)
          if (CACHES_TO_PRESERVE.indexOf(cacheName) === -1 && cacheName.startsWith("gih-cache-")) {
            log (`deleting cache ${ cacheName }`)
            caches.delete(cacheName)
          }
        })
      )
    }).catch(function(error) {
      log (`cache deletion error ${ error }`)
    })
  )
})











self.addEventListener('fetch', function(event) {
  let cached_content = null
  event.respondWith(
    caches.match(event.request).then(function(r) {
      const url = event.request.url
      if (r) {
        if (SWCONFIG.ENABLE_CACHE_HIT_LOG) {
          log (`cache hit ${url}`)
        }
        return r
      }

      const fetchRequest = event.request.clone()
      return fetch (fetchRequest, {cache: ("no-cache"})
        .then(function(response) {
          if (! (response.type === 'basic' || response.type === 'cors')) {
            return response
          }
          if (! (url.startsWith ("http://") || url.startsWith ("https://"))) {
            return response // chrome-extension:// etc.
          }
          if (fetchRequest.method !== 'GET') {
            return response
          }
          if(!response || response.status !== 200) {
            return response
          }

          const content_type = response.headers.get ('content-type') || ''
          if (response.type !== 'basic' && !matched (CORS_CACHE, url, content_type)) {
            return response
          }

          if (matched (DYN_NOCACHE, url, content_type)) {
            log (`skip dynamic url caching ${response.type} ${url}`)
            return response
          }

          if (response.type === 'basic' && matched (LIB_CACHE, url, content_type)) {
            return caches.open(LIB_CACHE_NAME).then(function(cache) {
              log (`caching ${response.type} ${url}`)
              cache.put(fetchRequest, response.clone ())
              return response
            })
          }
          else if (USE_CACHE && response.type === 'basic' && matched (DYN_CACHE, url, content_type)) {
            return caches.open(DYN_CACHE_NAME).then(function(cache) {
              log (`caching ${response.type} ${url}`)
              cache.put(fetchRequest, response.clone ())
              return response
            })
          }
          else if (response.type !== 'basic' && matched (CORS_CACHE, url, content_type)) {
            return caches.open(COR_CACHE_NAME).then(function(cache) {
              log (`caching ${response.type} ${url}`)
              cache.put(fetchRequest, response.clone ())
              return response
            })
          }
          else {
            if (SWCONFIG.ENABLE_CACHE_HIT_LOG) {
              log (`skip caching ${response.type} ${url}`)
            }
          }
          return response
        })
      })

      .catch (async function(error) {
        if (cached_content !== null) {
          log ('cache fetch error, reuse cached data')
          return cached_content
        }
        log ('cache fetch error')
        return caches.open (PRE_CACHE_NAME).then (function(cache) {
          if (event.request.headers.get ('accept').includes ('text/html')) {
            return cache.match ('/sw/offline.html')
          }
        })
      })
  )
})











// message pushing -------------------------------------
self.addEventListener('push', event => {
  log (`push ${ event.data.text() }`)
  const msg = event.data.json()
  const title = msg.data.title || 'Notification'
  const options = {
    body: msg.data.text,
    data: msg.data.link,
    icon: '/icon/128.png'
  }
  event.waitUntil(self.registration.showNotification(title, options));
})

self.addEventListener('notificationclick', function(event) {
  log ('push clicked')
  event.notification.close()
  event.waitUntil(clients.matchAll({
    type: 'window'
  }).then(function(clientList) {
    for (let i = 0; i < clientList.length; i++) {
      const client = clientList[i]
      if (client.url === '/' && 'focus' in client) {
        return client.focus()
      }
    }
    if (clients.openWindow) {
      return clients.openWindow(event.notification.data)
    }
  }))
})










// periodic sync ----------------------------------------



// sync --------------------------------------------------
const SYNC_HANDLERS = {
  "sync-test": async () => {
    await fetch ("/favicon.svg", {method: "GET"})
    console.log ('[sw.js] sync-test')
  },
}

self.addEventListener ('sync', event => {
  log (`sync event ${event.tag}`)
  if (event.tag in SYNC_HANDLERS) {
    event.waitUntil (SYNC_HANDLERS [event.tag] ())
  }
  else {
    consolr.log (`[sw.js] ${event.tag} handler not found`)
  }
})
