describe("Automation using Cypress", () => {
    it("Visit home page", () => {
        cy.writeFile ('cypress/fixtures/.tokens.json', {})
        cy.visit("/")
        cy.contains("KrackNet").should ("be.visible")
        cy.window ().then ((win) => {
            const tokens = {
                access_token: win.store.state.$ls.get ('access_token'),
                refresh_token: win.store.state.$ls.get ('access_token')
            }
            cy.writeFile ('cypress/fixtures/.tokens.json', tokens)
        })
        cy.get('body').then (($body) => {
            if ($body.find ('[href="/members/signin"]').length) {
                cy.log ('found sign in')
                cy.get ('[href="/members/signin"]').eq (0).click ()
                cy.wait (3000)
            } else {
                cy.log ('missing sign in')
            }
        })
    })
})
