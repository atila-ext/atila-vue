const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'zcmxoe',
  reporter: 'junit',
  reporterOptions: {
    mochaFile: 'cypress/results/output.xml',
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      // return require('./cypre`ss/plugins/index.js')(on, config)
    },
    baseUrl: 'http://skitai-dev:5000',
    modifyObstructiveCode: false,
  },
});
